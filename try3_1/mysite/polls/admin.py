from mongoadmin import site, DocumentAdmin

from app.documents import AppDocument

class AppDocumentAdmin(DocumentAdmin):
    pass
site.register(AppDocument, AppDocumentAdmin)