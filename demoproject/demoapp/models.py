from django.db import models

# Create your models here.

class Company(models.Model):
	company_name = models.CharField( max_length=255)
	store_name = models.CharField( max_length=255, unique=True)
	address =models.CharField(max_length = 200, blank= True)
	latitude = models.DecimalField(max_digits=6, decimal_places=3)
	longitude = models.DecimalField(max_digits=6, decimal_places=3)

	class Meta:
		ordering = ['-company_name']

	def __unicode__(self):
		return self.company_name, self.store_name